﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_задачи
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Задача 1");
            Console.WriteLine("Вычислить сумму 1+1/2+1/3+...+1/n.");
            Console.WriteLine();
            double i = 1, sum = 0;
            Console.Write("n = ");
            double n = double.Parse(Console.ReadLine());
            while (i <= n)
            {
                sum += 1 / i;
                i++;
            }
            Console.WriteLine(sum);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задача 2");
            Console.WriteLine("В ведомости указана зарплата, выплаченная каждому из сотрудников фирмы за месяц.\nОпределить общую сумму выплаченных по ведомости денег.");
            Console.WriteLine();
            i = 0;
            sum = 0;
            int zp = 0;
            Console.WriteLine("Введите количество сотрудников: ");
            n = int.Parse(Console.ReadLine());
            while (i < n)
            {
                Console.WriteLine($"Введите зарплату {i + 1}-го сотрудника: ");
                zp = int.Parse(Console.ReadLine());
                sum += zp;
                i++;
            }
            Console.WriteLine($"Суммарная зарплата {n} сотрудников составила {sum} рублей.");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задача 3");
            Console.WriteLine("Известны оценки по алгебре каждого ученика класса. Определить среднюю оценку.");
            Console.WriteLine();
            i = 0;
            sum = 0;
            zp = 0;
            double seredina = 0;
            Console.WriteLine("Введите количество учеников: ");
            n = int.Parse(Console.ReadLine());
            while (i < n)
            {
                Console.WriteLine($"Введите оценку {i + 1}-го ученика: ");
                zp = int.Parse(Console.ReadLine());
                sum += zp;
                i++;
            }
            seredina = sum / (double)n;
            Console.WriteLine($"Средняя оценка учеников составила: {seredina}.");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Задача 4");
            Console.WriteLine("Дана непустая последовательность целых чисел, оканчивающаяся числом 100. Определить, есть ли в последовательности число 77? Если имеются несколько таких чисел, то определить порядковый номер первого из них.");
            Console.WriteLine();
            i = 0;
            sum = 0;
            zp = 0;
            double flag = 0;
            n = 0;
            while (zp != 100)
            {
                Console.WriteLine($"Введите число ");
                zp = int.Parse(Console.ReadLine());
                if (zp == 77)
                {
                    sum++;
                    if (flag == 0)
                    {
                        flag = i;
                    }
                }
                i++;
            }
            if (sum == 0)
            {
                Console.WriteLine("Число 77 отсутствует во введенной последовательности");
            }
            else if (sum == 1)
            {
                Console.WriteLine("Число 77 присутствует во введенной последовательности");
            }
            else
            {
                Console.WriteLine($"Число 77 присутствует во введенной последовательности неоднократно, в первый раз на позиции {flag}.");
            }
            Console.ReadKey();
        }
    }
}
